@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="section">
        @include('notification')

        <!--   Icon Section   -->
            <div class="row">
                <div class="row center">
                    <div class="card">


                        <div class="row">
                            <div class="col s12">
                                <ul class="tabs">
                                    <li class="tab col s3"><a class="active" href="#test1">EXPLORE PROPERTIES</a></li>
                                </ul>
                            </div>
                            <div id="test1" class="col s12">

                                <div class="card-content grey lighten-4">

                                    <div class="input-field col s3">
                                        <select name="location">
                                            <option value="" disabled selected>Choose Location</option>
                                            @foreach($listings as $listing)
                                                <option value="">{{$listing->location}}</option>
                                            @endforeach

                                        </select>
                                        <label>Location</label>
                                    </div>


                                    <div class="input-field col s3">
                                        <select name="propertyType">
                                            <option value="" disabled selected>Choose Location</option>
                                            @foreach($listings as $listing)
                                                <option value="">{{$listing->propertyType}}</option>
                                            @endforeach
                                        </select>
                                        <label>Any property type</label>
                                    </div>


                                    <div class="input-field col s3">
                                        <select name="listingType">
                                            <option value="" disabled selected>Choose Location</option>
                                            @foreach($listings as $listing)
                                                <option value="">{{$listing->listingType}}</option>
                                            @endforeach
                                        </select>
                                        <label>Listing Type (Rent & Sale)</label>
                                    </div>


                                    <a class="btn waves-effect waves-light" href="#scroll"><i class="material-icons left">search</i> Search</a>
                                </div>


                            </div>


                        </div>



                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s6" >

                    @foreach($listings as $listing)
                        <div class="card details" >
                            <div class="row">
                                <div class="col s12 carousel">

                                    @foreach($listing->Images as $image)
                                        <a class="carousel-item" href="#">
                                            <img src="{{$image->url}}">
                                        </a>
                                    @endforeach

                                    <br>
                                </div>
                                <div class="col s12">
                                    <div class="s6">
                                        <div class="card-content">
                                            <span class="card-title center-align">{{$listing->title}}</span>
                                        </div>
                                        <div class="card-action">
                                            <a>&#x20A6; {{$listing->price}}</a>
                                            <a>{{$listing->location}}</a>
                                            <a class="waves-effect waves-light btn" href="{{url('report-listing/' . $listing->ltid)}}" style="margin-left:150px; ">Report<i class="material-icons left">flag</i></a>
                                        </div>
                                    </div>


                                </div>

                                <br> <br>



                                <div class="col s12" style="padding-left:250px;">
                                    <h5>Description</h5>
                                    <p>{{$listing->desc}}</p>
                                </div>

                                <br><br>
                                <div class="col s12" >
                                    <div class="col s6" >
                                        <p><b>Features</b></p>
                                        <ul>
                                            @foreach($listing->Features as $item)
                                                <li>{{$item->name}}</li>
                                            @endforeach

                                        </ul>

                                    </div>

                                    <div class="col s6" style="padding-left:155px;">
                                        <p><b>Facility</b></p>
                                        <ul>
                                            @foreach($listing->Facilities as $item)
                                                <li>{{$item->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <br><br>
                                <div class="col s12">

                                    <div class="col s6">
                                        <p>Published:</p>
                                        <p class="light">{{$listing->created_at}}</p>

                                    </div>

                                    <div class="col s6">
                                        <p>Listing ID:</p>

                                        <p class="light">{{$listing->listingId}}</p>
                                    </div>





                                </div>
                                <div class="col s12">


                                    <h5 class="center-align">Map</h5>



                                </div>



                            </div>
                        </div>
                    @endforeach


                </div>

                <div class="col s6">
                    <div class="card details2">

                        <div class="col s12">
                            <h5>Agency Name: {{$listing->Agent->agencyName}}</h5>
                        </div>
                        <br>


                        <div class="col s12" style="padding: 10px;">
                            <i class="material-icons left" style="color:#0085c3;">add_location</i> <h6> Agent Address:{{$listing->Agent->agencyAddress}}</h6>
                            <br>
                            <i class="material-icons left" style="color:#0085c3;">call</i> <h6> Phone:{{$listing->Agent->phone}}</h6>
                        </div>




                        <div class="col s12" style="margin-top: 30px;">
                            <h6><b>ASK ABOUT PROPERTY</b></h6>

                            <div class="row">
                                <form class="col s12" method="post" action="{{url('ask-email')}}">

                                    {{csrf_field()}}

                                    <input type="hidden" name="ltid" value="{{$listing->ltid}}">
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="first_name" name="fname" type="text" class="validate">
                                            <label for="first_name">First Name</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="last_name" name="lname" type="text" class="validate">
                                            <label for="last_name">Last Name</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="phone" name="phone" type="text" class="validate">
                                            <label for="phone">Phone</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="email" name="email" type="email" class="validate">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>

                                    <button class="waves-effect waves-light btn"><i class="material-icons left">chat_bubble</i>Email Agent</button>
                                </form>

                            </div>
                        </div>





                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h5 class="header col s12 light">A modern display of properties in the metropolis of the Federal capital Territory</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="images/banner3.jpg" alt="Unsplashed background img 3"></div>
    </div>


@endsection