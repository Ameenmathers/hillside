@extends('layouts.dashboard')

@section('content')

    <div class="row">
        <!-- side menu -->
        <div class="col s12 m4 l3 ">
            <ul id="slide-out" class="side-nav fixed blue-grey darken-1">
                <li>
                    <div class="user-view" style="background-color:#323232; color: lightblue;">
                        <div class="background">
                            <img class="circle" src="images/hillsidetiny.png">
                        </div>
                        <a href="#!user"><img class="circle" src="images/hillsidet.png"></a>
                        <a href="#!name"><span class="white-text name">John Doe</span></a>
                        <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                    </div>
                </li>


                <li><a href="{{url('/')}}"><i class="fa fa-home fa-2x" aria-hidden="true"></i>Home</a>
                <li><a href="{{url('admin-dashboard')}}"><i class="fa fa-user-circle fa-2x"
                                                            aria-hidden="true"></i>Dashboard</a>
                </li>
                <li><a href="{{url('usersTable')}}"><i class="fa fa-users fa-2x"></i>Users</a></li>
                <li><a href="{{url('agencyTable')}}"><i class="fa fa-id-badge fa-2x"></i>Agencies</a></li>
                <li class="active"><a href="{{url('listingsView')}}"><i class="fa fa-home fa-2x"></i>Listings</a></li>
                <li><a href="{{url('adminSettings')}}"><i class="fa fa-cog fa-2x"></i>Settings</a></li>


                <li>
                    <div class="divider"></div>
                </li>
                <li><a class="waves-effect" href="{{url('logout')}}"><i class="fa fa-power-off fa-2x"></i>Logout</a>
                </li>


            </ul>
        </div>

        <!-- end side menu -->


        <div class="col s12 m8 l9">

            @include('notification')

            <br><br><br>

            <div class="row left-align" style="padding: 10px;">
                <h5><i class="fa fa-home fa fa-2x"></i> Listings</h5>

            </div>
            <div class="row">


                <table class="striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Published Date</th>
                        <th>Property type</th>
                        <th>Listing type</th>
                        <th>View</th>
                        <th>Delete Record</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $listings as $listing)
                        <tr>
                            <td>{{$listing->title}}</td>
                            <td>{{$listing->location}}</td>
                            <td>{{$listing->created_at}}</td>
                            <td>{{$listing->propertyType}}</td>
                            <td>{{$listing->listingType}}</td>
                            <td>
                                <a href="{{url('propertyDetail/' . $listing->ltid)}}" class="btn waves-light"
                                   name="action">View
                                    <i class="material-icons right">search</i>
                                </a>
                            </td>
                            <td>
                                <a href="{{url('delete-listing/' . $listing->ltid) }}" class="btn waves-light red"
                                   type="submit" name="action">Delete
                                    <i class="material-icons right">delete</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>


            </div>
            <br><br>
            <div class="row center-align">
                <ul class="pagination">
                    {{$listings->links()}}
                </ul>

            </div>


        </div>


    </div>


@endsection