@extends('layouts.dashboard')

@section('content')

    @include('notification')
    <div class="row">

        <!-- side menu -->
        <div class="col s12 m4 l3 ">
            <ul id="slide-out" class="side-nav fixed blue-grey darken-1">
                <li>
                    <div class="user-view" style="background-color:#323232; color: lightblue;">
                        <div class="background">
                            <img class="circle" src="images/hillsidetiny.png">
                        </div>
                        <a href="#!user"><img class="circle" src="images/hillsidet.png"></a>
                        <a href="#!name"><span class="white-text name">John Doe</span></a>
                        <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                    </div>
                </li>


                <li><a href="{{url('/')}}"><i class="fa fa-home fa-2x" aria-hidden="true"></i>Home</a>
                <li class="active"><a href="{{url('admin-dashboard')}}"><i class="fa fa-user-circle fa-2x"
                                                                           aria-hidden="true"></i>Dashboard</a>
                </li>
                <li><a href="{{url('usersTable')}}"><i class="fa fa-users fa-2x"></i>Users</a></li>
                <li><a href="{{url('agencyTable')}}"><i class="fa fa-id-badge fa-2x"></i>Agencies</a></li>
                <li><a href="{{url('listingsView')}}"><i class="fa fa-home fa-2x"></i>Listings</a></li>
                <li><a href="{{url('adminSettings')}}"><i class="fa fa-cog fa-2x"></i>Settings</a></li>


                <li>
                    <div class="divider"></div>
                </li>
                <li><a class="waves-effect" href="{{url('logout')}}"><i class="fa fa-power-off fa-2x"></i>Logout</a>
                </li>


            </ul>
        </div>

        <!-- end side menu -->

        <div class="col s12 m8 l9">


            <br><br><br>
            <div class="row">

                <div class="col s4">
                    <div class="card-panel teal"
                         style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1;">
                        <span class="white-text center-align"><i class="fa fa-users fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Users</span>
                                <h4 class="right-align">{{count($users)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('usersTable')}}">View Users</a>
                                {{--<a href="{{url('register')}}">Add User</a>--}}
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel"
                         style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#b0395c;">
                        <span class="white-text center-align"><i class="fa fa-home fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Listings</span>
                                <h4 class="right-align">{{count($listings)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('listingsView')}}">View Listing</a>
                                {{--<a href="{{url('offerProperty')}}">Add Listing</a>--}}

                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel"
                         style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#425b44;">
                        <span class="white-text center-align"><i class="fa fa-id-badge fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Agencies</span>
                                <h4 class="right-align">{{count($agents)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('agencyTable')}}">View</a>
                                {{--<a href="{{url('register')}}">Add Agency</a>--}}
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>
            </div>


            <div class="row">

                <div class="col s4">

                    <div class="card">
                        <div class="card-content">
                            <form method="post" action="{{url('add-facility')}}">
                                {{ csrf_field() }}
                                <div class="input-field s4 {{ $errors->has('name') ? ' has-error' : '' }}">

                                    <input id="facilities" name="name" type="text" class="validate" required>
                                    <label for="facilities" class="active">Facility Add</label>
                                </div>

                        </div>
                        <div class="card-action">
                            <button name="submit" class="waves-effect waves-light btn" style=" margin-top: 20px;">Add
                                Facility
                            </button>
                        </div>
                        </form>
                    </div>


                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card">
                        <div class="card-content">
                            <form method="post" action="{{url('add-feature')}}">
                                {{ csrf_field() }}
                                <div class="input-field s4 {{ $errors->has('name') ? ' has-error' : '' }}">

                                    <input id="features" name="name" type="text" class="validate" required>
                                    <label for="features" class="active">Feature Add</label>
                                </div>

                        </div>
                        <div class="card-action">
                            <button name="submit" class="waves-effect waves-light btn" style=" margin-top: 20px;">Add
                                Feature
                            </button>
                        </div>
                        </form>
                    </div>


                </div>

                <div class="col s4">

                    <div class="card">


                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>
            </div>

            <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.2843856067334!2d7.501957414486105!3d9.037802093514754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0bed2344ed7b%3A0x82adf1bba69930e2!2sAguleri+St%2C+Garki%2C+Abuja%2C+Nigeria!5e0!3m2!1sen!2s!4v1505947561010"
                        width="900" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>


        </div>


    </div>




@endsection