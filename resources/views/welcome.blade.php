<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h3 class="header center teal-text text-lighten-2">Hillside Properties limited</h3>
                <div class="row center">
                    <h5 class="header col s12 light">A modern display of properties in the metropolis of the Federal capital
                        Territory</h5>
                </div>

                <br><br>

            </div>
        </div>
        <div class="parallax"><img src="images/banner1.png" alt="Unsplashed background img 1"></div>
    </div>

    <br><br>


    <div class="container" style="position:absolute; top:35%; left: 16%; z-index: 4;">


        <div class="row center">
            <div class="card">


                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3"><a class="active" href="#test1">EXPLORE PROPERTIES</a>
                                @if(Input::has("location"))
                                    <label class="label label-success">Filtered By Location</label>
                                @endif

                            </li>
                        </ul>
                    </div>
                    <div id="test1" class="col s12">

                        <div class="card-content grey lighten-4">

                            <form action="{{url('/search')}}">
                                <div class="input-field col s3">
                                    <select name="location">
                                        <option value="" disabled selected>Choose Location</option>
                                        @foreach($listings as $listing)
                                            <option>{{$listing->location}}</option>
                                        @endforeach

                                    </select>
                                    <label>Location</label>
                                </div>


                                <div class="input-field col s3">
                                    <select name="propertyType">
                                        <option value="" disabled selected>Select Property Type</option>
                                        @foreach($listings as $listing)
                                            <option>{{$listing->propertyType}}</option>
                                        @endforeach
                                    </select>
                                    <label>Any property type</label>
                                </div>


                                <div class="input-field col s3">
                                    <select name="listingType">
                                        <option value="" disabled selected>Choose Listing Type</option>
                                        @foreach($listings as $listing)
                                            <option>{{$listing->listingType}}</option>
                                        @endforeach
                                    </select>
                                    <label>Listing Type (Rent & Sale)</label>
                                </div>


                                <button class="btn waves-effect waves-light"><i class="material-icons left">search</i>
                                    Search
                                </button>
                            </form>
                        </div>


                    </div>


                </div>


            </div>
        </div>
    </div>


    <div class="container">
        <div class="section">

            <!--   Icon Section   -->

            <div class="row">

                @foreach($listings as $listing)
                    <div class="col s12 m6 l3" style="margin-right:80px;">
                        <div class="card medium2 z-depth-5">
                            <div class="card-image1">
                                <img src="{{$listing->Images[0]->url}}">

                            </div>
                            <div class="card-content">
                                <span class="card-title">{{$listing->title}}</span>
                            </div>
                            <div class="card-action">
                                <a href="#">&#x20A6; {{$listing->price}}</a>
                                <a href="{{url('propertyDetail/' . $listing->ltid)}}">Show details</a>
                            </div>
                        </div>

                    </div>

                @endforeach


            </div>


        </div>
    </div>


    <div class="container3">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="fa fa-handshake-o" aria-hidden="true"></i></h2>
                        <h5 class="center">Buy & Rent</h5>

                        <p class="light">
                            Standard properties at competitive prices are offered for sale or for rent. Organized according
                            to location, property type, and offer type, Hillside Properties enables you to browse through
                            multiple property listings and contact numerous real estate agents or owners from the comfort of
                            your home.
                        </p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="fa fa-home" aria-hidden="true"></i></h2>
                        <h5 class="center">Find Your Property</h5>

                        <p class="light">
                            Searching for the perfect property, whether for residential or commercial purposes, need not be
                            stressful and time-consuming. Whether you are looking for a house, a flat, a plot of land,
                            commercial space, or properties for development,
                        </p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="fa fa-user" aria-hidden="true"></i></h2>
                        <h5 class="center">Know Your Agent</h5>

                        <p class="light">Our aim is to provide convenience and trust for property seekers by also offering
                            quality support via chat, email and phone..</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h5 class="header col s12 light">A modern display of properties in the metropolis of the Federal capital
                        Territory</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="images/banner2.jpg" alt="Unsplashed background img 2"></div>
    </div>


    <div class="col s12 center">


        <div class="col s6 card-panel z-depth-5">
            <h4><b>Contact Us</b></h4>
            <br><br>
            <div class="row">
                <form class="col s6" action="{{url('welcome')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix" type="text" class="validate">
                            <label for="icon_prefix">Full Name</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone</i>
                            <input id="icon_telephone" type="tel" class="validate">
                            <label for="icon_telephone">Telephone</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                            <label for="icon_prefix2">Message</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input id="icon_prefix3" type="email" class="validate">
                            <label for="icon_prefix3">Email</label>
                        </div>
                    </div>

                    <a class="waves-effect waves-light btn"><i class="material-icons left">chat_bubble</i>Email Us</a>
                </form>
                <div class="col s6">

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.2843856067334!2d7.501957414486105!3d9.037802093514754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0bed2344ed7b%3A0x82adf1bba69930e2!2sAguleri+St%2C+Garki%2C+Abuja%2C+Nigeria!5e0!3m2!1sen!2s!4v1505947561010"
                            width="600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>


    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h5 class="header col s12 light">A modern display of properties in the metropolis of the Federal capital
                        Territory</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="images/banner3.jpg" alt="Unsplashed background img 3"></div>
    </div>



@endsection