@extends('layouts.app')

@section('content')

    <div class="container2" style="background-color:#fff;">
        <div class="section">

            <!--   Icon Section   -->


            <div class="row">

                <form style="padding: 15px;" method="POST" action="{{ url('register') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="role" value="user">

                    <div class="row">
                        <div class="col s12 m6 l6" align="center">
                            <h5 style="color:#00365b; margin-left:-15px;">CUSTOMER REGISTRATION</h5>
                            <span class="psw">Are you an agent? <a href="{{url('agent-register')}}">Register Here</a></span>
                        </div>
                    </div>

                    <div class="col s12 m4 l8">
                        <div class="input-field col s4 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix1" type="email" name="email" value="{{ old('email') }}"
                                   class="validate" required>
                            <label for="icon_prefix1">Email</label>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-field col s4 {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <i class="material-icons prefix">call</i>
                            <input id="icon_prefix1" type="text" name="phone" class="validate"
                                   value="{{ old('phone') }}" required pattern="[0-9]+">
                            <label for="icon_prefix1">Phone Number</label>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col s12 m4 l8">
                        <div class="input-field col s4 {{ $errors->has('fname') ? ' has-error' : '' }}">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix1" type="text" name="fname" value="{{ old('fname') }}"
                                   class="validate" required>
                            <label for="icon_prefix1">First Name</label>

                            @if ($errors->has('fname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="input-field col s4 {{ $errors->has('lname') ? ' has-error' : '' }}">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix1" type="text" name="lname" value="{{ old('lname') }}"
                                   class="validate" required>
                            <label for="icon_prefix1">Last Name</label>

                            @if ($errors->has('lname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                            @endif
                        </div>


                    </div>


                    <div class="col s12 m6 l8">
                        <div class="input-field col s6 {{ $errors->has('password') ? ' has-error' : '' }}">
                            <i class="material-icons prefix">https</i>
                            <input id="password" type="password" name="password" class="validate" required>
                            <label for="password">Password</label>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="col s12 m6 l8">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">https</i>
                            <input id="password-confirm" name="password_confirmation" type="password" class="validate"
                                   required>
                            <label for="password">Password Confirmation</label>
                            <br><br>
                            <button name="submit" type="submit" class="waves-effect waves-light btn"
                                    style="margin-left: 380px;">Register
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12" style=" margin-top:-25px;">
                            <span class="psw">Already have an account? <a href="{{url('login')}}">Login Now</a></span>
                        </div>
                    </div>


                </form>

                <div class="headerDivider z-depth-5"></div>


                <div class="choice">
                    <h5>Welcome to Hillside Properties</h5>
                    <p>In just minutes you will be able to select your perfect home.</p>
                    <div class="divider"></div>
                    <ul>
                        <li><a><i class="fa fa-home fa-3x"></i></a><b>Numerous properties</b></li>
                        <p>View properties for sale and rent near you</p>
                        <li><a><i class="fa fa-th-list fa-3x"></i></a><b> Clever Listings</b></li>
                        <p>View properties for sale and rent near you.</p>
                    </ul>
                </div>

            </div>


        </div>
    </div>


@endsection