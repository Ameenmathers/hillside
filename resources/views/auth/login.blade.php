@extends('layouts.auth')

@section('content')

    <div class="container2">
        <br>
        <div class="section">

            <style>
                .help-block{
                    color:red;
                }
            </style>
            <!--   Icon Section   -->

            <div class="card-panel hoverable" style="width:400px; position: relative; left:35%;">
                <div class="row">
                    <form class="col s12" method="POST" action="{{ url('login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col s12 m4 l8">
                                <h6><a href="{{url('welcome')}}"><i class="fa fa-arrow-left"></i> Home</a></h6>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l8" style="margin-left:70px; margin-top: -35px;">

                                <h5 style="color:#00365b; margin-left:45px;">SIGN IN</h5>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_prefix" type="email" name="email" class="validate" value="{{ old('email') }}" required>
                                <label for="icon_prefix">Email</label>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>


                        <div class="row">
                            <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }} ">
                                <i class="material-icons prefix">https</i>
                                <input id="password" type="password" name="password" class="validate" required>
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <p>
                                    <input id="test5" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="test5">Remember me</label>
                                </p>

                                <div class="col s12" style="margin-left:180px; margin-top:-25px;">
                                    <span class="psw">Forgot <a href="{{ url('password.request') }}">password?</a></span>
                                </div>


                            </div>
                        </div>

                        <div class="divider"></div>
                        <div class="row">
                            <div class="input-field col s12 m4 l2">
                                <button name="submit" class="waves-effect waves-light btn" style=" width: 320px; background-color:#00365b
;">Login</button>

                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col s12" style="margin-left:35px; margin-top:-25px;">
                                <span class="psw">Don't have an account? <a href="{{ url('register') }}">Register Now</a></span>
                            </div>
                        </div>






                    </form>
                </div>
            </div>

        </div>
        <br><br><br>
    </div>


@endsection
