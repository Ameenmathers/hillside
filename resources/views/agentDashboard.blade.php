@extends('layouts.app')

@section('content')

    <br><br>

    <div class="row col s12">
        <div class="card-panel teal" style="height: 110px;">
            <div class="col s12">
                <div class="col s4 white-text">
                    <i class="fa fa-id-badge fa-2x"></i>
                    <br>
                    <span class="card-title"><b>Agency Name:</b>  {{Auth::user()->agencyName}}</span>

                </div>
                <div class="col s4 white-text">
                    <i class="fa fa-list fa-2x"></i>
                    <br>
                    <span class="card-title"><b>Listings:</b> {{$listings}}</span>

                </div>
                <div class="col s4 white-text">
                    <i class="fa fa-location-arrow fa-2x"></i>
                    <br>
                    <span class="card-title"><b>Location:</b> {{Auth::user()->agencyAddress}}</span>
                </div>

            </div>


        </div>

    </div>

    <div class="row">

        <div class="col s6 card-panel">
            <br>
            <h5>Password Change</h5>
            <form  style="padding: 15px;" method="post" action="">


                <div class="col s12 m4 l8">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">https</i>
                        <input id="password" type="password" class="validate" required>
                        <label for="password">Old Password</label>
                    </div>
                </div>


                <div class="col s12 m4 l8">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">https</i>
                        <input id="password" type="password" class="validate" required>
                        <label for="password">New Password</label>
                    </div>
                </div>

                <div class="col s12 m4 l8">

                    <div class="input-field col s12">
                        <i class="material-icons prefix">https</i>
                        <input id="confirm-password" type="password" class="validate" required>
                        <label for="password">Password Confirmation</label>


                    </div>
                </div>


                <div class="col s12">
                    <button name="submit" class="waves-effect waves-light btn">Change</button>
                    <button name="submit" class="waves-effect waves-light btn" type="reset" value="reset">Reset</button>


                </div>

                <div class="row">
                    <div class="col s12">

                    </div>
                </div>



            </form>
        </div>
        <div class="col s6 card-panel">
            <br>
            <h5>Account Change</h5>
            <form  style="padding: 15px;" method="post" action="{{url('agentProfile')}}">
                {{ csrf_field() }}


                <div class="col s12 m4 l8">
                    <div class="input-field col s12  {{ $errors->has('agencyName') ? ' has-error' : '' }}">
                        <input id="agencyName" type="text" name="agencyName" value="{{Auth::user()->agencyName}}" class="validate" required>
                        <label for="agencyName">Agency Name</label>
                    </div>
                </div>


                <div class="col s12 m4 l8">
                    <div class="input-field col s12  {{ $errors->has('agencyWebsite') ? ' has-error' : '' }}">

                        <input id="agencyWebsite" type="text" name="agencyWebsite" value="{{Auth::user()->agencyWebsite}}" class="validate">
                        <label for="agencyWebsite">Agency Website</label>
                    </div>
                </div>

                <div class="col s12 m4 l8">

                    <div class="input-field col s12  {{ $errors->has('agencyAddress') ? ' has-error' : '' }}">
                        <i class="material-icons prefix">https</i>
                        <input id="agencyAddress" type="text" name="agencyAddress" value="{{Auth::user()->agencyAddress}}" class="validate" required>
                        <label for="agencyAddress">Agency Address</label>


                    </div>
                </div>


                <div class="col s12">
                    <button name="submit" class="waves-effect waves-light btn">Change</button>
                    <button class="waves-effect waves-light btn" type="reset" value="reset">Reset</button>


                </div>

                <div class="row">
                    <div class="col s12">

                    </div>
                </div>



            </form>
        </div>



    </div>



@endsection