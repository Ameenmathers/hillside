
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Hillside Properties Limited</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{url('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
</head>
<body style="background-image: url('images/banner5.jpg');">

@include('notification')
@yield('content')

<!--  Scripts-->
<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/materialize.js')}}"></script>
<script src=" {{url('js/init.js')}}"></script>
<script type="text/javascript">  $(document).ready(function() {$('select').material_select();
    });
</script>
<script type="text/javascript">$('select').material_select('destroy');</script>
<script type="text/javascript">
    $('#textarea1').val('New Text');
    $('#textarea1').trigger('autoresize');
</script>


</body>
</html>



