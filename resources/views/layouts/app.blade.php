<?php use Illuminate\Support\Facades\Input; ?>
        <!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" contentjquery.min.js="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Hillside Properties Limited</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{url('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">

</head>
<body>
<nav class="white" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="{{ url('/welcome') }}" class="brand-logo">Hillside Properties</a>

        <ul id="dropdown1" class="dropdown-content" style="margin-top: 60px;">

            @if(!Auth::guest())
                <li><a href="{{ url('/') }}">Home<i class="material-icons left">home</i></a></li>
                @if(Auth::user()->role == "agent")
                    <li><a href="{{url('offerProperty')}}">Offer Property <i class="material-icons left">add_box</i></a></li>
                @endif
                <li><a href="{{url('logout')}}">Logout</a></li>
            @endif
        </ul>

        <ul class="right hide-on-med-and-down">
            @if(!Auth::guest())
                @if(Auth::user()->role == "agent")
                <li><a class="" href="{{ url('offerProperty') }}">Offer Property<i class="material-icons left">add_box</i></a></li>
                    <li><a class="" href="{{ url('dashboard') }}">Dashboard<i class="material-icons left">dashboard</i></a></li>
                    <li><a class="" href="{{ url('agentListing') }}">Listings<i class="material-icons left">account_box</i></a></li>
                @endif

                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">
                        Logged in as {{Auth::user()->fname}} {{Auth::user()->lname}}
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                </li>
            @else
                <li><a href="{{ url('login') }}">Login</a></li>
                <li><a href="{{ url('register') }}">Register</a></li>
            @endif
        </ul>


        <ul id="nav-mobile" class="side-nav">
            @if(!Auth::guest())

                @if(Auth::user()->role == "agent")
                    <li><a class="" href="{{ url('offerProperty') }}">Offer Property<i class="material-icons left">add_box</i></a></li>
                    <li><a class="" href="{{ url('dashboard') }}">Dashboard<i class="material-icons left">dashboard</i></a></li>
                    <li><a class="" href="{{ url('agentListing') }}">Listings<i class="material-icons left">account_box</i></a></li>
                @endif

                <li>Logged in as {{Auth::user()->fname}} {{Auth::user()->lname}}</li>
                <li><a href="{{ url('/logout') }}"> Logout</a></li>
            @else
                <li><a class="waves-effect waves-light btn" href="{{ url('login') }}">Login</a></li>
                <li><a href="{{ url('register') }}">Register</a></li>

            @endif
        </ul>

        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>


@include('notification')
@yield('content')


<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Hillside Properties Limited</h5>
                <p class="grey-text text-lighten-4">View properties for sale and rent near you. With high resolution photo galleries and easy search tools at your fingertips.
                </p>

            </div>

            <div class="col l6 offset-12 s12" style="padding-left:300px;">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i>  Hillsideproperties</a></li>
                    <li><a class="white-text" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Hill@gmail.com</a></li>
                    <li><a class="white-text" href="#"><i class="fa fa-twitter" aria-hidden="true"></i>  Hillsideproperties</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="brown-text text-lighten-3" href="http://materializecss.com"></a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/materialize.js')}}"></script>
<script src="{{url('js/init.js')}}"></script>
<script type="text/javascript">  $(document).ready(function() {$('select').material_select();
    });
</script>
<script type="text/javascript">$('select').material_select('destroy');</script>
<script type="text/javascript">
    $('#textarea1').val('New Text');
    $('#textarea1').trigger('autoresize');
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
</script>


</body>
</html>
