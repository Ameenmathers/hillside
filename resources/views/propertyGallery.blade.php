@extends('layouts.app')

@section('content')

    <br><br>

    <div class="container">
        <div class="row center">
            <div class="card">


                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3"><a class="active" href="#test1">EXPLORE PROPERTIES</a></li>
                        </ul>
                    </div>
                    <div id="test1" class="col s12">

                        <div class="card-content grey lighten-4">

                            <div class="input-field col s3">
                                <select name="location">
                                    <option value="" disabled selected>Choose Location</option>
                                    @foreach($listings as $listing)
                                        <option value="">{{$listing->location}}</option>
                                    @endforeach

                                </select>
                                <label>Location</label>
                            </div>


                            <div class="input-field col s3">
                                <select name="propertyType">
                                    <option value="" disabled selected>Select Property Type</option>
                                    @foreach($listings as $listing)
                                        <option value="">{{$listing->propertyType}}</option>
                                    @endforeach
                                </select>
                                <label>Any property type</label>
                            </div>


                            <div class="input-field col s3">
                                <select name="listingType">
                                    <option value="" disabled selected>Choose Listing Type</option>
                                    @foreach($listings as $listing)
                                        <option value="">{{$listing->listingType}}</option>
                                    @endforeach
                                </select>
                                <label>Listing Type (Rent & Sale)</label>
                            </div>


                            <a class="btn waves-effect waves-light" href="#scroll"><i class="material-icons left">search</i> Search</a>
                        </div>


                    </div>


                </div>



            </div>
        </div>
    </div>


    <br>




    <div class="container">
        <div class="section">

            <!--   Icon Section   -->

            <div class="row">
                @foreach($listings as $listing)
                    <div class="col s12 m6 l3" style="margin-right:80px;">
                        <div class="card medium2 z-depth-5">
                            <div class="card-image1">
                                <img src="{{$listing->Images[0]->url}}">

                            </div>
                            <div class="card-content">
                                <span class="card-title">{{$listing->title}}</span>
                            </div>
                            <div class="card-action">
                                <a href="#">&#x20A6; {{$listing->price}}</a>
                                <a href="{{url('propertyDetail/' . $listing->ltid)}}">Show details</a>
                            </div>
                        </div>

                    </div>

                @endforeach



            </div>


        </div>
    </div>





    <div class="container">
        <div class="section">

            {{$listings->links()}}

        </div>

    </div>






    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h5 class="header col s12 light">A modern display of properties in the metropolis of the Federal capital Territory</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="images/banner3.jpg" alt="Unsplashed background img 3"></div>
    </div>





@endsection