@extends('layouts.app')

@section('content')

    <br><br>
    <div class="row card-panel">

        <form  style="padding: 15px;" method="post" enctype="multipart/form-data" action="{{url('offerProperty')}}">
            {{ csrf_field() }}

            <div class="row">
                <div class="col s12 m4 l8" style="margin-left:70px;">

                    <h5 style="color:#00365b; margin-left:-50px;">OFFER PROPERTY</h5>
                </div>
            </div>
            <div class="col s12 m4 l8">
                <div class="input-field col s4 {{ $errors->has('title') ? ' has-error' : '' }}">
                    <input id="propertyTitle" name="title" type="text" class="validate" required>
                    <label for="propertyTitle">Property Title</label>
                </div>
                <div class="input-field col s4 {{ $errors->has('location') ? ' has-error' : '' }}">
                    <input id="propertyLocation" name="location" type="text" class="validate" required>
                    <label for="propertyLocation">Property Location</label>
                </div>
            </div>
            <div class="col s12 m4 l8">
                <div class="input-field col s4 {{ $errors->has('price') ? ' has-error' : '' }}">
                    <input id="price" type="text" name="price" class="validate" required>
                    <label for="price">Property Price</label>
                </div>

                <div class="input-field col s4 {{ $errors->has('listingid') ? ' has-error' : '' }}">
                    <input id="listingid" type="text" name="listingid" class="validate" required>
                    <label for="listingid">Lisitng ID</label>
                </div>


            </div>
            <div class="col s12 m4 l8">
                <div class="input-field col s4 {{ $errors->has('propertyType') ? ' has-error' : '' }}">
                    <select  name="propertyType">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="flat">Flat</option>
                        <option value="house">House</option>
                        <option value="land">Land</option>
                        <option value="development">Development</option>
                    </select>
                    <label>Property Type</label>
                </div>
                <div class="input-field col s4 {{ $errors->has('listingType') ? ' has-error' : '' }}">
                    <select name="listingType">
                        <option value="" disabled selected >Choose your option</option>
                        <option value="rent">Rent</option>
                        <option value="sale">Sale</option>
                    </select>
                    <label>Rent or Sale</label>
                </div>
            </div>
            <div class="col s12 m4 l8">
                <div class="input-field col s4 inline {{ $errors->has('facilities') ? ' has-error' : '' }}">
                    <select name="fcid[]" multiple>

                        @foreach($facilities as $item)
                            <option value="{{$item->fcid}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    <label for="facilities">Facilities</label>
                </div>


                <div class="input-field col s4 inline {{ $errors->has('features') ? ' has-error' : '' }}">
                    <select name="ftid[]" multiple>

                        @foreach($features as $item)
                            <option value="{{$item->ftid}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    <label for="features">Features</label>


                </div>

            </div>

            <div class="col s12 m4 l8">
                <div class="input-field col s4 {{ $errors->has('nobed') ? ' has-error' : '' }}">

                    <input id="nobed" type="text" name="nobed" class="validate" required>
                    <label for="nobed">Number of bedrooms</label>

                </div>
                <div class="input-field col s4 {{ $errors->has('nobath') ? ' has-error' : '' }}">

                    <input id="nobath" type="text" name="nobath" class="validate" required>
                    <label for="nobath">Number of bathrooms</label>


                </div>

                <br><br>

            </div>
            <div class="col s12 m4 l8">

                <div class="file-field input-field col s8">
                    <div class="btn">
                        <span>Picture</span>
                        <input type="file" name="images[]" multiple>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate"  type="text" placeholder="Upload one or more files">
                    </div>
                </div>

            </div>
            <div class="col s12 m4 l8">

                <div class="input-field col s8 {{ $errors->has('desc') ? ' has-error' : '' }}">
                    <i class="material-icons prefix">mode_edit</i>
                    <textarea id="desc" class="materialize-textarea" name="desc"></textarea>
                    <label for="desc"> Description</label>
                </div>
            </div>



            <div class="col s12 m4 l8">
                <button name="submit" class="waves-effect waves-light btn" style="margin-left: 350px; margin-top: 20px;">Add Property</button>


            </div>




        </form>

        <br><br>

    </div>

@endsection