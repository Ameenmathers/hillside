@extends('layouts.dashboard')

@section('content')

    <div class="row">

        <!-- side menu -->
        <div class="col s12 m4 l3 ">
            <ul id="slide-out" class="side-nav fixed blue-grey darken-1">
                <li>
                    <div class="user-view" style="background-color:#323232; color: lightblue;">
                        <div class="background">
                            <img class="circle" src="images/hillsidetiny.png">
                        </div>
                        <a href="#!user"><img class="circle" src="images/hillsidet.png"></a>
                        <a href="#!name"><span class="white-text name">John Doe</span></a>
                        <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                    </div>
                </li>


                <li><a href="{{url('/')}}"><i class="fa fa-home fa-2x" aria-hidden="true"></i>Home</a>
                <li><a href="{{url('admin-dashboard')}}"><i class="fa fa-user-circle fa-2x"
                                                            aria-hidden="true"></i>Dashboard</a>
                </li>
                <li><a href="{{url('usersTable')}}"><i class="fa fa-users fa-2x"></i>Users</a></li>
                <li><a href="{{url('agencyTable')}}"><i class="fa fa-id-badge fa-2x"></i>Agencies</a></li>
                <li><a href="{{url('listingsView')}}"><i class="fa fa-home fa-2x"></i>Listings</a></li>
                <li class="active"><a href="{{url('adminSettings')}}"><i class="fa fa-cog fa-2x"></i>Settings</a></li>

                <li>
                    <div class="divider"></div>
                </li>
                <li><a class="waves-effect" href="{{url('logout')}}"><i class="fa fa-power-off fa-2x"></i>Logout</a>
                </li>


            </ul>
        </div>

        <!-- end side menu -->

        <div class="col s12 m8 l9">

            @include('notification')

            <br><br><br>

            <div class="row">
                <div id="page-wrapper">
                    <div class="row">

                        <div class="col-lg-12">
                            <h5 class="page-header">Settings</h5>

                        </div>

                    </div>

                    <div class="col s8 card-panel">
                        <br>
                        <h5>Password Change</h5>
                        <form style="padding: 15px;" method="post" action="{{url('adminSettings')}}}">
                            {{ csrf_field() }}

                            <div class="col s12 m4 l8">
                                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="material-icons prefix">https</i>
                                    <input id="password" type="password" name="password" class="validate" required>
                                    <label for="password">Old Password</label>
                                </div>
                            </div>


                            <div class="col s12 m4 l8">
                                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="material-icons prefix">https</i>
                                    <input id="password" type="password" name="password" class="validate" required>
                                    <label for="password">New Password</label>
                                </div>
                            </div>

                            <div class="col s12 m4 l8">

                                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="material-icons prefix">https</i>
                                    <input id="confirm-password" type="password" class="validate" required>
                                    <label for="password">Password Confirmation</label>


                                </div>
                            </div>


                            <div class="col s12">
                                <button name="submit" class="waves-effect waves-light btn" style="margin-left: 300px;">
                                    Change
                                </button>
                                <button class="waves-effect waves-light btn" type="reset" value="reset">Reset</button>


                            </div>

                            <div class="row">
                                <div class="col s12">

                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>


        </div>


    </div>


@endsection