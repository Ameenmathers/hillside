<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <title>Agent Listing</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{url('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">

</head>

<body>

<!-- Navbar goes here -->
<nav class="white" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="{{ url('/welcome') }}" class="brand-logo">Hillside Properties</a>
        @if (Route::has('login'))
            <ul class="right hide-on-med-and-down">
                @auth
                <a href="{{ url('/') }}"> {{Auth::user()->name}}</a>

                @else
                    <li ><a href="{{url('agentProfile')}}"><i class="fa fa-user"></i> My Account</a></li>
                    <li class="active"><a href="{{url('agentListing')}}"><i class="fa fa-list"></i> Listings</a></li>
                    <li><a href="{{url('logout')}}">Logout</a></li>
                    <li><a class="waves-effect waves-light btn" href="{{url('offerProperty')}}">Offer Property<i class="material-icons left">add_box</i></a></li>
                    @endauth
            </ul>

        @endif


        @if (Route::has('login'))
            <ul id="nav-mobile" class="side-nav">
                @auth
                <a href="{{ url('/agentProfile') }}"><i class="fa fa-user"></i> {{Auth::user()->fname}} {{Auth::user()->lname}}</a>
                <li class="active"><a href="{{url('agentListing')}}"><i class="fa fa-list"></i> Listings</a></li>
                <li><a href="{{url('logout')}}">Logout</a></li>
                <li><a class="waves-effect waves-light btn" href="{{url('offerProperty')}}">Offer Property<i class="material-icons left">add_box</i></a></li>
                @else


                    @endauth
            </ul>

        @endif
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<br><br>
<div class="row">

    <div class="col s12"> <!-- Note that "m8 l9" was added -->
        <table class="striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Price</th>
                <th>Listing ID</th>
                <th>Published Date</th>
                <th>Description</th>
                <th>Features</th>
                <th>Facility</th>
                <th>Property type</th>
                <th>Action</th>

            </tr>
            </thead>

            <tbody>
            @foreach($listings as $listing)
            <tr>
                <td>{{$listing->title}}</td>
                <td>{{$listing->price}}</td>
                <td>{{$listing->listingId}}</td>
                <td>{{$listing->created_at}}</td>
                <td>{{$listing->features}}</td>
                <td>{{$listing->facilities}}</td>
                <td>{{$listing->propertyType}}</td>
                <td>
                    <a href="{{url('delete-agentListing/' . $listing->ltid) }}" class="btn waves-light red" type="submit" name="action">Delete
                            <i class="material-icons right">delete</i>
                        </a></td>


            </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <br><br>
    <div class="row center-align">
        <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>

    </div>


</div>
<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Hillside Properties Limited</h5>
                <p class="grey-text text-lighten-4">View properties for sale and rent near you. With high resolution photo galleries and easy search tools at your fingertips.
                </p>

            </div>

            <div class="col l6 offset-12 s12" style="padding-left:300px;">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#!"><i class="fa fa-facebook-official" aria-hidden="true"></i>  Hillsideproperties</a></li>
                    <li><a class="white-text" href="#!"><i class="fa fa-google-plus" aria-hidden="true"></i> Hill@gmail.com</a></li>
                    <li><a class="white-text" href="#!"><i class="fa fa-twitter" aria-hidden="true"></i>  Hillsideproperties</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="brown-text text-lighten-3" href="http://materializecss.com"></a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{url('js/materialize.js')}}"></script>
<script src="{{url('js/init.js')}}"></script>
<script type="text/javascript">  $(document).ready(function() {$('select').material_select();
});
</script>
<script type="text/javascript">$('select').material_select('destroy');</script>
<script type="text/javascript">
    $('#textarea1').val('New Text');
    $('#textarea1').trigger('autoresize');
</script>



</body>


<!-- Mirrored from demo.geekslabs.com/materialize-v1.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 08:33:57 GMT -->
</html>