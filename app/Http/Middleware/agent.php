<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class agent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if(Auth::user()->role == 'agent' || Auth::user()->role == 'admin')
            return $next($request);
    	else{
    		$request->session()->flash("error","You are not authorized to access that page.");
    		return redirect("/");
	    }
    }
}
