<?php

namespace App\Http\Controllers;

use App\listing;
use App\Mail\AskEmail;
use App\Mail\reportListing;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    //
    public function index()
    {
        if(!Auth::guest()){
            if(Auth::user()->role == "Admin") return redirect('adminView');
            if(Auth::user()->role == "Agent") return redirect('agentProfile');

        }

        $listings = listing::all();
        return view('welcome',[
            'listings' => $listings
        ]);
    }

	public function agentRegister() {
		return view('agentRegister');
    }

    public function propertyDetail($ltid)
    {
        $listings = listing::where('ltid',$ltid)->get();
        $agencyTable = User::where('role','agent')->get();

        return view('propertyDetail',[
            'listings'=> $listings,
            'agents' => $agencyTable
        ]);

    }

    public function propertyGallery()
    {
        $listings = listing::all();
        return view('propertyGallery',[
            'listings' => $listings
        ]);
    }


    public function reportListing(Request $request, $ltid)
    {
    	try{
		    $listing = listing::find($ltid);
		    Mail::to('info@hillsideproperties.com.ng')->send(new reportListing($listing));

		    $request->session()->flash('success', "Listing Reported");

		    return redirect('propertyDetail/' . $ltid);

	    }   catch (\Exception $exception){

    		$request->session()->flash('error','Sorry an error occurred. Please try again.');
		    return redirect('propertyDetail/' . $ltid);

	    }

    }

	public function askEmail( Request $request ) {

    	try{

    		$ltid = $request->input('ltid');
    		$listing = listing::find($ltid);

		    Mail::to($listing->Agent->email)->send(new AskEmail($request->all()));


		    $request->session()->flash('success','Email Sent Successfully.');

		    return redirect('propertyDetail/' . $ltid);
	    }catch (\Exception $exception){
    		$request->session()->flash('error','Sorry an error occurred. Please try again.');
		    return redirect('propertyDetail/' . $ltid);
	    }


    }

	public function contactEmail( Request $request ) {

    	try{

		$ltid = $request->input('ltid');
		$listing = listing::find($ltid);

		Mail::to("info@hillsideproperties.com.ng")->send(new AskEmail($request->all()));

		$request->session()->flash('success','Email Sent Successfully.');

		return redirect('propertyDetail/' . $ltid);
	    }catch (\Exception $exception){
		    $request->session()->flash('error','Sorry an error occurred. Please try again.');
		    return redirect('propertyDetail/' . $ltid);

	    }


	}

    public function search()
    {
      $location     =  Input::get('location');
      $propertyType =   Input::get('propertyType');
      $listingType  =  Input::get('listingType');

        $listing = listing::where('location',$location)->where('propertyType',$propertyType)->where('listingType',$listingType)->paginate(20);

        return view('propertyGallery',[
            'listings' => $listing
        ]);
    }


}
