<?php

namespace App\Http\Controllers;

use App\facilities;
use App\features;
use App\listing;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function agencyTable()
    {

        $agencyTable = User::where('role','agent')->paginate(20);

        return view('agencyTable',[
            'agents' => $agencyTable
        ]);
    }

    public function usersTable()
    {
        $usersTable = User::where('role','user')->paginate(20);

        return view('usersTable',[
            'users' => $usersTable
        ]);
    }

    public function listingsView()
    {
        $listings = listing::latest()->paginate(20);
        return view('listingsView',[
            'listings'=> $listings
        ]);


    }

	public function adminDashboard()
	{
		$listings = listing::all();
		$agents = User::where('role','agent')->get();
		$users = User::where('role','user')->get();
		return view('adminDashboard',[
			'listings' => $listings,
			'agents' => $agents,
			'users' => $users

		]);
	}


    public function adminSettings()
    {
        return view('adminSettings');
    }


    public function deleteListing(Request $request, $ltid)
    {
        listing::destroy($ltid);

        $request->session()->flash('success','Listed Deleted.');
        return redirect('listingsView');
    }

    public function deleteUser(Request $request, $uid)
    {
        user::destroy($uid);

        $request->session()->flash('success','User Deleted.');
        return redirect('usersTable');
    }

    public function deleteAgency(Request $request,$uid)
    {
        user::destroy($uid);

        $request->session()->flash('success','Agency Deleted.');
        return redirect('agencyTable');
    }

	public function postAddFacility(Request $request){
		try{
			$facility = new facilities();
			$facility->name = $request->input('name');
			$facility->save();

			$request->session()->flash('success','Facility Added.');

			return redirect( 'admin-dashboard');

		}catch (\Exception $exception){

			$request->session()->flash('error','Sorry an error occurred. Please try again');
			return redirect( 'admin-dashboard');
		}
	}

	public function postAddFeature(Request $request){

		try{
			$feature = new features();
			$feature->name = $request->input('name');
			$feature->save();

			$request->session()->flash('success','Feature Added.');

			return redirect( 'admin-dashboard');

		}catch (\Exception $exception) {
			$request->session()->flash( 'error', 'Sorry an error occurred. Please try again' );
			return redirect( 'admin-dashboard' );
		}


	}


}
