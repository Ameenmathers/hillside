<?php

namespace App\Http\Controllers;

use App\facilities;
use App\features;
use App\image;
use App\listing;
use App\listing_facilities;
use App\listing_features;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AgentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agent');
    }

    public function offerProperty()
    {
        $facilities = facilities::all();
        $features = features::all();

        return view('offerProperty',[
            'facilities' => $facilities,
            'features' => $features
        ]);



    }

    public function postOfferProperty(Request $request){

    	try{
		    $fcid = $request->input('fcid');
		    $ftid = $request->input('ftid');


		    $offerProperty = new listing();
		    $offerProperty->listingid = $request->input('listingid');
		    $offerProperty->title = $request->input('title');
		    $offerProperty->location = $request->input('location');
		    $offerProperty->propertyType = $request->input('propertyType');
		    $offerProperty->listingType = $request->input('listingType');
		    $offerProperty->price = $request->input('price');
		    $offerProperty->desc = $request->input('desc');
		    $offerProperty->nobed = $request->input('nobed');
		    $offerProperty->nobath = $request->input('nobath');
		    $offerProperty->nobath = $request->input('nobath');
		    $offerProperty->uid = Auth::user()->uid;
		    $offerProperty->save();

		    foreach ( $request->file( 'images' ) as $item ) {
			    $rand          = Str::random( 5 );
			    $inputFileName = $item->getClientOriginalName();
			    $item->move( "uploads", $rand . $inputFileName );

			    $image      = new image();
			    $image->url = url( 'uploads/' . $rand . $inputFileName );
			    $image->ltid = $offerProperty->ltid;
			    $image->save();
		    }

		    foreach($fcid as $item){
			    $facility = new listing_facilities();
			    $facility->fcid = $item;
			    $facility->ltid = $offerProperty->ltid;
			    $facility->save();
		    }

		    foreach($ftid as $item){
			    $feature = new listing_features();
			    $feature->ftid = $item;
			    $feature->ltid = $offerProperty->ltid;
			    $feature->save();
		    }

		    $request->session()->flash('success','Listing Added.');

		    return redirect( 'offerProperty');

	    }catch (\Exception $exception){

    		$request->session()->flash('error','Sorry an error occurred. Please try again');
		    return redirect( 'offerProperty');

	    }


    }

    public function agentListing()
    {
        $listings = listing::all();
        $agencyTable = User::where('role','agent')->get();

        return view('agentListing',[
            'listings'=> $listings,
            'agents' => $agencyTable
        ]);
    }

    public function agentProfile()
    {
        $listings = listing::all()->count();
        return view('agentProfile',[
            'listings' => $listings
        ]);
    }


	public function agentDashboard()
	{
		$listings = listing::all()->count();
		return view('agentDashboard',[
			'listings' => $listings
		]);
	}


	public function postAgentProfile(Request $request)
    {
        try{
            $agent = User::find(Auth::user()->uid);
            $agent->agencyName = $request->input('agencyName');
            $agent->agencyWebsite = $request->input('agencyWebsite');
            $agent->agencyAddress = $request->input('agencyAddress');
            $agent->save();
            $request->session()->flash('success','Profile Updated');
        } catch(\Exception $exception){
            $request->session()->flash('error','Sorry An Error Occurred.');
        }

        return redirect('agentProfile');
    }


    public function deleteAgentListing($ltid)
    {
        listing::destroy($ltid);

        return redirect('agentListing');
    }


    //
}
