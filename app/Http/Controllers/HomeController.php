<?php

namespace App\Http\Controllers;

use App\facilities;
use App\features;
use App\listing_facilities;
use App\listing_features;
use App\User;
use App\listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function dashboard() {


		if(Auth::user()->role == 'agent') return redirect('agent-dashboard');
		if(Auth::user()->role == 'admin') return redirect('admin-dashboard');

		return redirect('user-dashboard');
    }

	public function logout() {
		Auth::logout();
		return redirect('/');
    }
}
