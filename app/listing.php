<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class listing extends Model
{
    protected  $table = 'listing';
    protected $primaryKey ='ltid';


    public function Facilities ()
    {
        return $this->hasManyThrough(facilities::class,listing_facilities::class, 'ltid','fcid', 'ltid');
    }


    public function Features ()
    {
        return $this->hasManyThrough(features::class,listing_features::class, 'ltid','ftid', 'ltid');
    }

    public function Agent()
    {
      return $this->belongsTo(User::class,'uid','uid');
    }

    public function images()
    {
      return $this->hasMany(image::class,'ltid','ltid');
    }

}

