<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HillsideDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email',128)->index();
            $table->string('token')->index();
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('fname');
            $table->string('lname');
            $table->string('phone');
            $table->string('agencyName');
            $table->string('agencyWebsite')->nullable();
            $table->string('agencyAddress');
            $table->string('email',128)->unique();
            $table->string('password');
            $table->enum('role', array('user','admin','agent'));
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('listing', function (Blueprint $table) {
            $table->increments('ltid');
            $table->string('listingId');
            $table->string('title');
            $table->string('location');
            $table->enum('propertyType', array('flat','house','land','development'));
            $table->enum('listingType', array('sale','rent'));
            $table->integer('price');
            $table->string('desc',2000);
            $table->integer('nobed');
            $table->integer('nobath');
            $table->integer('uid');
            $table->timestamps();
        });

        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('fcid');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('images', function (Blueprint $table) {
            $table->increments('imid');
            $table->string('url',2000);
            $table->integer('ltid');
            $table->timestamps();
        });

        Schema::create('features', function (Blueprint $table) {
            $table->increments('ftid');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('listing_facilities', function (Blueprint $table) {
            $table->increments('lfcid');
            $table->integer('fcid');
            $table->integer('ltid');
            $table->timestamps();
        });

        Schema::create('listing_features', function (Blueprint $table) {
            $table->increments('lfid');
            $table->integer('ftid');
            $table->integer('ltid');
            $table->timestamps();
        });






        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('users');
        Schema::dropIfExists('listing');
        Schema::dropIfExists('facilities');
        Schema::dropIfExists('features');
        Schema::dropIfExists('listing_facilities');
        Schema::dropIfExists('listing_features');


        //
    }
}
