<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', 'PublicController@index');
Route::get('/welcome', 'PublicController@index');
Route::get('/home', 'PublicController@index');
Route::get('/search','PublicController@search');
Route::get('/agent-register','PublicController@agentRegister');

Route::get('/report-listing/{ltid}','PublicController@reportListing');
Route::get('/delete-listing/{ltid}','AdminController@deleteListing');
Route::get('/delete-agentListing/{ltid}','AgentController@deleteAgentListing');
Route::get('/delete-user/{uid}','AdminController@deleteUser');
Route::get('/delete-agency/{uid}','AdminController@deleteAgency');

Route::get('logout','HomeController@logout');

Route::post('ask-email','PublicController@askEmail');

Route::get('/offerProperty','AgentController@offerProperty');
Route::get('/agencyTable','AdminController@agencyTable');
Route::get('/usersTable','AdminController@usersTable');

Route::get('/listingsView','AdminController@listingsView');
Route::get('/userAdd','HomeController@userAdd');
Route::get('/propertyDetail/{ltid}','PublicController@propertyDetail');
Route::get('/propertyGallery','PublicController@propertyGallery');
Route::get('/adminView','AdminController@adminView');
Route::post('add-facility', 'AdminController@postAddFacility');
Route::post('add-feature', 'AdminController@postAddFeature');
Route::post('offerProperty', 'AgentController@postOfferProperty');



Route::get('/adminSettings','AdminController@adminSettings');
Route::get('/agentListing/','AgentController@agentListing');
Route::get('/agentProfile','AgentController@agentProfile');
Route::post('/agentProfile','AgentController@postAgentProfile');



Route::get('dashboard','HomeController@dashboard');


//admin routes

Route::get('admin-dashboard','AdminController@adminDashboard');
// agent routes

Route::get('agent-dashboard','AgentController@agentDashboard');



